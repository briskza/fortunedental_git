package com.fortunedental.fortunedental.fragment;

/**
 * Created by acount on 19/4/2559.
 */

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;


import com.fortunedental.fortunedental.Customviewgroup.ListPatientScheduleCustomViewGroup;
import com.fortunedental.fortunedental.DAO.DAO_DoctorSchedule;

import com.fortunedental.fortunedental.DAO.DAO_PatientSchedule;
import com.fortunedental.fortunedental.R;
import com.fortunedental.fortunedental.activity.DescriptionScheduleActivity;
import com.fortunedental.fortunedental.adapter.ScheduleDoctorListAdapter;
import com.fortunedental.fortunedental.adapter.SchedulePatientListAdapter;
import com.fortunedental.fortunedental.util.JsonParser_DoctorSchedule;
import com.fortunedental.fortunedental.util.JsonParser_PatientSchedule;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ScheduleFragment extends Fragment {

    private ListView listView;
    private ScheduleDoctorListAdapter listAdapterD;
    private SchedulePatientListAdapter listAdapterP;
    private List<DAO_DoctorSchedule> daoD;
    private AQuery aq;
    private static AlertDialog.Builder ad;
    private TextView emptyViewGroup;
    private TextView tvEmpty;
    private List<DAO_PatientSchedule> daoP;

    public interface FragmentDoctorScheduleListener {
        void onDoctorScheduleClicked(DAO_DoctorSchedule dao);
    }

    public ScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_schedule, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootview) {

        // Init 'View' instance(s) with rootView.findViewById here
        listView = (ListView) rootview.findViewById(R.id.listView);


        aq = new AQuery(getActivity());
        ad = new AlertDialog.Builder(getActivity());
        ad.setPositiveButton("ตกลง", null);

        Schedule(rootview);


    }

    private void Schedule(final View rootview) {
        Intent i = getActivity().getIntent();
        Bundle bundle = i.getBundleExtra("infoBundle");
        String permission = bundle.getString("permission");

        Map<String, String> param = new HashMap<>();

        //Schedule Doctor
        if (permission.equals("0")) {
            String idDoctor = bundle.getString("idDoctor");
            param.put("id_doctor", idDoctor);
            aq.ajax("http://27.254.63.25/fortune_dental_api/api/v1/fd/ScheduleDoctorNew", param, JSONObject.class, new AjaxCallback<JSONObject>() {
                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    if (json != null) {
                        try {
                            if (json.get("result").toString().equals("1")) {
                                //found data

                                final List<DAO_DoctorSchedule> dataSchedule = JsonParser_DoctorSchedule.parseSchedule(getActivity(), json);
                                //Toast.makeText(getActivity(), "dataSchedule"+dataSchedule, Toast.LENGTH_SHORT).show();
                                daoD = dataSchedule;
                                listAdapterD = new ScheduleDoctorListAdapter(dataSchedule);
                                listView.setAdapter(listAdapterD);
                                listView.setOnItemClickListener(listViewItemClickListenerDoctor);

                            } else {
                                //not found dana
                                tvEmpty = (TextView) rootview.findViewById(R.id.tvEmpty);
                                tvEmpty.setText("ไม่มีตารางนัดหมายของคุณหมอ");


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
//                 //ajax error, show error code
//                 UtilsApp.snackbarAlertNetwork(mContext, ActivityLogin.this);
                    }
                }
            });


        } else if (permission.equals("1")) {
            // emptyViewGroup = (TextView) rootview.findViewById(R.id.tvEmptyScheduleCustomViewGroup);
            String idPatient = bundle.getString("idPatient");
            param.put("id_patient", idPatient);

            aq.ajax("http://27.254.63.25/fortune_dental_api/api/v1/fd/SehedulePatientNew", param, JSONObject.class, new AjaxCallback<JSONObject>() {


                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    if (json != null) {
                        try {
                            if (json.get("result").toString().equals("1")) {
                                //found data

                                final List<DAO_PatientSchedule> dataSchedule = JsonParser_PatientSchedule.parseSchedule(getActivity(), json);
                                daoP = dataSchedule;
                                listAdapterP = new SchedulePatientListAdapter(dataSchedule);
                                listView.setAdapter(listAdapterP);
                                listView.setOnItemClickListener(listViewItemClickListenerPatient);


                            } else {
                                //not found dana

                                tvEmpty = (TextView) rootview.findViewById(R.id.tvEmpty);
                                tvEmpty.setText("ไม่มีตารางนัดหมายของคนไข้");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
//                 //ajax error, show error code
//                 UtilsApp.snackbarAlertNetwork(mContext, ActivityLogin.this);
                    }
                }
            });

        }


    }

    AdapterView.OnItemClickListener listViewItemClickListenerDoctor = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //get permission
            Intent i = getActivity().getIntent();
            Bundle bundle = i.getBundleExtra("infoBundle");
            String permission = bundle.getString("permission");
            //Toast.makeText(getActivity(), permission, Toast.LENGTH_SHORT).show();

            //send passing data in descriptionSchedule
            Intent intentDescriptionSchedule = new Intent(getActivity(), DescriptionScheduleActivity.class);

            Bundle b = new Bundle();
            b.putString("idPatient",daoD.get(position).idPatient);
            b.putString("date",daoD.get(position).date);
            b.putString("time",daoD.get(position).time);
            b.putString("type",daoD.get(position).type);
            b.putString("permissionSchedule",permission);
            intentDescriptionSchedule.putExtra("infoMoreSchedule", b);

            startActivity(intentDescriptionSchedule);

            //Toast.makeText(getActivity(), daoD.get(position).name, Toast.LENGTH_SHORT).show();
            // parent.getSelectedItem()
//            DAO_DoctorSchedule dao;
//            FragmentDoctorScheduleListener listener = (FragmentDoctorScheduleListener) getActivity();
//            listener.onDoctorScheduleClicked(dao);
        }
    };



    AdapterView.OnItemClickListener listViewItemClickListenerPatient = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //get permission
            Intent i = getActivity().getIntent();
            Bundle bundle = i.getBundleExtra("infoBundle");
            String permission = bundle.getString("permission");

            //send passing data in descriptionSchedule
            Intent intentDescriptionSchedule = new Intent(getActivity(), DescriptionScheduleActivity.class);

            Bundle b = new Bundle();
            b.putString("idPatient",daoP.get(position).idPatient);
            b.putString("nameDoctor",daoP.get(position).name);
            b.putString("surnameDoctor",daoP.get(position).sname);
            b.putString("date",daoP.get(position).date);
            b.putString("time",daoP.get(position).time);
            b.putString("type",daoP.get(position).type);
            b.putString("permissionSchedule",permission);
            intentDescriptionSchedule.putExtra("infoMoreSchedule", b);

            startActivity(intentDescriptionSchedule);
        }
    };
}

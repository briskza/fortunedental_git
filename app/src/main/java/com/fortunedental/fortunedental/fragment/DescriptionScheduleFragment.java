package com.fortunedental.fortunedental.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fortunedental.fortunedental.Customviewgroup.ViewDescriptionScheduleCustomViewGroup;
import com.fortunedental.fortunedental.R;
import com.fortunedental.fortunedental.activity.AppointmentDateActivity;
import com.fortunedental.fortunedental.activity.MainActivity;

/**
 * Created by acount on 7/5/2559.
 */
public class DescriptionScheduleFragment extends Fragment implements View.OnClickListener {

    private TextView tvPosition;
    private TextView tvIdPatient;
    private TextView tvDatetime;
    private TextView tvEvent;
    private String permission;
    private ViewDescriptionScheduleCustomViewGroup viewgroup;
    com.prolificinteractive.materialcalendarview.MaterialCalendarView calendarView;
    private Button btnAppointment;
    private AlertDialog.Builder ad;

    public DescriptionScheduleFragment() {
        super();
    }

    public static DescriptionScheduleFragment newInstance() {
        DescriptionScheduleFragment fragment = new DescriptionScheduleFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = null;

        Intent i = getActivity().getIntent();

        Bundle bundle = i.getBundleExtra("infoMoreSchedule");
        permission = bundle.getString("permissionSchedule");


        if (permission.equals("0")) {
            rootView = inflater.inflate(R.layout.fragment_description_doctor_schedule, container, false);
            initInstances(rootView);

        } else if (permission.equals("1")) {
            rootView = inflater.inflate(R.layout.fragment_description_patient_schedule, container, false);
            initInstances(rootView);
        }

        //View rootView;
        return rootView;
    }

    private void initInstances(View rootView) {
        ad = new AlertDialog.Builder(getActivity());
        Intent i = getActivity().getIntent();

        Bundle bundle = i.getBundleExtra("infoMoreSchedule");

        if (permission.equals("0")) {
            String idPatient = bundle.getString("idPatient");
            String date = bundle.getString("date");
            String time = bundle.getString("time");
            String event = bundle.getString("type");

            // Init 'View' instance(s) with rootView.findViewById here
            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG1_doctor);
            viewgroup.setTextTopicCustomViewGroup("หมายเลขคนไข้");
            viewgroup.setTexttvDescriptionCustomViewGroup(idPatient);

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG2_doctor);
            viewgroup.setTextTopicCustomViewGroup("วันที่นัดคนไข้");
            viewgroup.setTexttvDescriptionCustomViewGroup(date);

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG3_doctor);
            viewgroup.setTextTopicCustomViewGroup("เวลาที่นัดคนไข้");
            viewgroup.setTexttvDescriptionCustomViewGroup(time);

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG4_doctor);
            viewgroup.setTextTopicCustomViewGroup("ตรวจ");
            viewgroup.setTexttvDescriptionCustomViewGroup(event);

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG5_doctor);
            viewgroup.setTextTopicCustomViewGroup("");
            viewgroup.setTexttvDescriptionCustomViewGroup("");
            viewgroup.setHideivFirstaid();

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG6_doctor);
            viewgroup.setTextTopicCustomViewGroup("");
            viewgroup.setTexttvDescriptionCustomViewGroup("");
            viewgroup.setHideivFirstaid();

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG7_doctor);
            viewgroup.setTextTopicCustomViewGroup("");
            viewgroup.setTexttvDescriptionCustomViewGroup("");
            viewgroup.setHideivFirstaid();

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG8_doctor);
            viewgroup.setTextTopicCustomViewGroup("");
            viewgroup.setTexttvDescriptionCustomViewGroup("");
            viewgroup.setHideivFirstaid();


        } else if (permission.equals("1")) {
            String idPatient = bundle.getString("idPatient");
            String date = bundle.getString("date");
            String time = bundle.getString("time");
            String event = bundle.getString("type");
            String nameDoctor = bundle.getString("nameDoctor");
            String surnameDoctor = bundle.getString("surnameDoctor");

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG1_patient);
            viewgroup.setTextTopicCustomViewGroup("หมายเลขคนไข้");
            viewgroup.setTexttvDescriptionCustomViewGroup(idPatient);

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG2_patient);
            viewgroup.setTextTopicCustomViewGroup("วันที่นัดหมาย");
            viewgroup.setTexttvDescriptionCustomViewGroup(date);

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG3_patient);
            viewgroup.setTextTopicCustomViewGroup("เวลาที่นัด");
            viewgroup.setTexttvDescriptionCustomViewGroup(time);

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG4_patient);
            viewgroup.setTextTopicCustomViewGroup("หมอที่ดูแล");
            viewgroup.setTexttvDescriptionCustomViewGroup(nameDoctor + " " + surnameDoctor);

            viewgroup = (ViewDescriptionScheduleCustomViewGroup) rootView.findViewById(R.id.vdsCVG5_patient);
            viewgroup.setTextTopicCustomViewGroup("ตรวจ");
            viewgroup.setTexttvDescriptionCustomViewGroup(event);

            btnAppointment = (Button) rootView.findViewById(R.id.btnAppointment);
            btnAppointment.setOnClickListener(this );


        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnAppointment) {
            ad.setTitle("การเลื่อนนัด");
            ad.setMessage("คุณต้องการเลื่อนนัด หรือไม่ ?");
            ad.setPositiveButton("ต้องการ", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getActivity(), AppointmentDateActivity.class);
                    startActivity(intent);
                    //finish();
                }
            });
            ad.setNegativeButton("ไม่ต้องการ", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                }
            });
            ad.show();
        }
    }


}

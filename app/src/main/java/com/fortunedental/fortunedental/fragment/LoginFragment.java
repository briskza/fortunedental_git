package com.fortunedental.fortunedental.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.fortunedental.fortunedental.DAO.DAO_DoctorProfile;
import com.fortunedental.fortunedental.DAO.DAO_PatientProfile;
import com.fortunedental.fortunedental.R;
import com.fortunedental.fortunedental.activity.MainActivity;
import com.fortunedental.fortunedental.util.JsonParser_PatientProfile;
import com.fortunedental.fortunedental.util.JsonParser_DoctorProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by acount on 15/4/2559.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    int someVar;
    private TextView tvPricacy;
    private Button btnLogin;
    private AQuery aq;
    private EditText editTextUser;
    private EditText editTextPwd;
    private static AlertDialog.Builder ad;
    private static Context mContext;
    private ProgressDialog dia;

    public static LoginFragment newInstance(int someVar) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putInt("someVar", someVar);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Read from Arguments
        someVar = getArguments().getInt("someVar");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        //findViewById here
        tvPricacy = (TextView) rootView.findViewById(R.id.tvPricacy);
        btnLogin = (Button) rootView.findViewById(R.id.btnLogin);
        editTextUser = (EditText) rootView.findViewById(R.id.editTextUser);
        editTextPwd = (EditText) rootView.findViewById(R.id.editTextPwd);

        aq = new AQuery(getActivity());
        ad = new AlertDialog.Builder(getActivity());
        ad.setPositiveButton("ตกลง", null);
        dia = new ProgressDialog(getActivity());

        btnLogin.setOnClickListener(this);
    }

    public void setHelloText(String text) {
        tvPricacy.setText(text);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //save state here
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            //restore state here
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnLogin) {
            if (formValidation()) {
                login();
            }
        }
    }

    private void login() {
        dia.setMessage("กำลังเข้าสู่ระบบ..");
        dia.setCancelable(false);
        dia.show();

        final Map<String, String> param = new HashMap<>();

        param.put("username", editTextUser.getText().toString());
        param.put("password", editTextPwd.getText().toString());


        aq.progress(dia).ajax("http://27.254.63.25/fortune_dental_api/api/v1/fd/AllLoginTest", param, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if (json != null) {
                    try {
                        if (json.get("result").toString().equals("1")) {
                            if (json.get("permission").toString().equals("0")) {
                                final List<DAO_DoctorProfile> dataloginDoctor = JsonParser_DoctorProfile.parseLogin(getActivity(), json);
                                //Toast.makeText(getActivity(), "username:" + dataloginDoctor.get(0).username + " name:" + dataloginDoctor.get(0).name, Toast.LENGTH_SHORT).show();

                                Intent i = new Intent(getActivity(), MainActivity.class);

                                Bundle b = new Bundle();
                                b.putString("idDoctor", dataloginDoctor.get(0).idDoctor);
                                b.putString("name", dataloginDoctor.get(0).name);
                                b.putString("sname", dataloginDoctor.get(0).sname);
                                b.putString("bday", dataloginDoctor.get(0).bday);
                                b.putString("age", dataloginDoctor.get(0).age);
                                b.putString("sex", dataloginDoctor.get(0).sex);
                                b.putString("address", dataloginDoctor.get(0).address);
                                b.putString("tel", dataloginDoctor.get(0).tel);
                                b.putString("idCard", dataloginDoctor.get(0).idCard);
                                b.putString("username", dataloginDoctor.get(0).username);
                                b.putString("passwrd", dataloginDoctor.get(0).password);

                                b.putString("permission", json.get("permission").toString());
                                i.putExtra("infoBundle", b);

                                startActivity(i);
                                getActivity().finish();
                            } else if (json.get("permission").toString().equals("1")) {
                                final List<DAO_PatientProfile> datalogin = JsonParser_PatientProfile.parseLogin(getActivity(), json);
                                //Toast.makeText(getActivity(), "username:" + datalogin.get(0).username + " name:" + datalogin.get(0).name, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(getActivity(), MainActivity.class);

                                Bundle b = new Bundle();
                                b.putString("idPatient", datalogin.get(0).idPatient);
                                b.putString("name", datalogin.get(0).name);
                                b.putString("sname", datalogin.get(0).sname);
                                b.putString("bday", datalogin.get(0).bday);
                                b.putString("age", datalogin.get(0).age);
                                b.putString("sex", datalogin.get(0).sex);
                                b.putString("address", datalogin.get(0).address);
                                b.putString("tel", datalogin.get(0).tel);
                                b.putString("idCard", datalogin.get(0).idCard);
                                b.putString("username", datalogin.get(0).username);
                                b.putString("passwrd", datalogin.get(0).password);
                                b.putString("idDoctor", datalogin.get(0).idDoctor);
                                b.putString("permission", json.get("permission").toString());
                                i.putExtra("infoBundle", b);

                                //Toast.makeText(getActivity(), "permission:" +json.get("permission"), Toast.LENGTH_SHORT).show();
                                startActivity(i);
                                getActivity().finish();
                            }
                        } else {
                            ad.setTitle("เข้าสู่ระบบ");
                            ad.setCancelable(false);
                            ad.setMessage(json.get("message").toString());
                            ad.show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
//                    //ajax error, show error code
////                    UtilsApp.snackbarAlertNetwork(mContext, ActivityLogin.this);
                    ad.setTitle("เข้าสู่ระบบ");
                    ad.setCancelable(false);
                    ad.setMessage("กรุณาเชื่อมต่ออินเตอร์เน็ตด้วยค่ะ");
                    ad.show();
                }

            }
        });


    }

    private boolean formValidation() {

        // Check Username
        if (editTextUser.getText().length() == 0) {
            ad.setTitle("เข้าสู่ระบบ");
            ad.setMessage("โปรดใส่ชื่อผู้ใช้ให้ครบ");
            ad.show();
            editTextUser.requestFocus();
            return false;
        }

        // Check Password
        if (editTextPwd.getText().length() == 0) {
            ad.setTitle("เข้าสู่ระบบ");
            ad.setMessage("โปรดใส่พาสเวิร์ดให้ครบ");
            ad.show();
            editTextPwd.requestFocus();
            return false;
        }


        return true;
    }
}

package com.fortunedental.fortunedental.fragment;

/**
 * Created by acount on 19/4/2559.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.fortunedental.fortunedental.Customviewgroup.ListProfileCustomViewGroup;
import com.fortunedental.fortunedental.DAO.DAO_DoctorProfile;
import com.fortunedental.fortunedental.R;


import java.util.List;


public class ProfileFragment extends Fragment {

    private TextView tvUsernameProfile;
    private TextView tvNameProfile;
    private ListProfileCustomViewGroup viewgroup;



    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootview) {

        Intent i = getActivity().getIntent();

        Bundle bundle = i.getBundleExtra("infoBundle");

        String permission = bundle.getString("permission");


        if (permission.equals("1")) {

            String name = bundle.getString("name");
            String username = bundle.getString("username");
            String sname = bundle.getString("sname");
            String bday = bundle.getString("bday");
            String age = bundle.getString("age");
            String sex = bundle.getString("sex");
            String address = bundle.getString("address");
            String tel = bundle.getString("tel");
            String idCard = bundle.getString("idCard");
            String idDoctor = bundle.getString("idDoctor");

            tvNameProfile = (TextView) rootview.findViewById(R.id.tvNameProfile);
            tvNameProfile.setText(name + " " + sname);

            tvUsernameProfile = (TextView) rootview.findViewById(R.id.tvUsernameProfile);
            tvUsernameProfile.setText("@" + username);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup1);
            viewgroup.setTextViewTopic("วันเกิด");
            viewgroup.setTextViewDetail(bday);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup2);
            viewgroup.setTextViewTopic("อายุ");
            viewgroup.setTextViewDetail(age);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup3);
            viewgroup.setTextViewTopic("เพศ");
            if (sex.equals("1")) {
                viewgroup.setTextViewDetail("ชาย");
            } else if (sex.equals("2")) {
                viewgroup.setTextViewDetail("หญิง");
            }

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup4);
            viewgroup.setTextViewTopic("ที่อยู่");
            viewgroup.setTextViewDetail(address);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup5);
            viewgroup.setTextViewTopic("เบอร์โทร");
            viewgroup.setTextViewDetail(tel);

//            viewgroup4 = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup6);
//            viewgroup4.setTextViewTopic("รหัสประชาชน");
//            viewgroup4.setTextViewDetail(idCard);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup6);
            viewgroup.setTextViewTopic("รหัสหมอที่ดูแล");
            viewgroup.setTextViewDetail(idDoctor);
            if (idDoctor.equals("1")) {
                viewgroup.setTextViewDetail("คุณหมอณพงษ์ พัวพรพงษ์");
            } else if (idDoctor.equals("2")) {
                viewgroup.setTextViewDetail("คุณหมอรุจิรา วงศ์เบญจืทรัพย์");
            }
        } else if (permission.equals("0")) {
            String name = bundle.getString("name");
            String username = bundle.getString("username");
            String sname = bundle.getString("sname");
            String bday = bundle.getString("bday");
            String age = bundle.getString("age");
            String sex = bundle.getString("sex");
            String address = bundle.getString("address");
            String tel = bundle.getString("tel");
            String idCard = bundle.getString("idCard");

            tvNameProfile = (TextView) rootview.findViewById(R.id.tvNameProfile);
            tvNameProfile.setText(name + " " + sname);

            tvUsernameProfile = (TextView) rootview.findViewById(R.id.tvUsernameProfile);
            tvUsernameProfile.setText("@" + username);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup1);
            viewgroup.setTextViewTopic("วันเกิด");
            viewgroup.setTextViewDetail(bday);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup2);
            viewgroup.setTextViewTopic("อายุ");
            viewgroup.setTextViewDetail(age);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup3);
            viewgroup.setTextViewTopic("เพศ");
            if (sex.equals("1")) {
                viewgroup.setTextViewDetail("ชาย");
            } else if (sex.equals("2")) {
                viewgroup.setTextViewDetail("หญิง");
            }

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup4);
            viewgroup.setTextViewTopic("ที่อยู่");
            viewgroup.setTextViewDetail(address);

            viewgroup = (ListProfileCustomViewGroup) rootview.findViewById(R.id.viewGroup5);
            viewgroup.setTextViewTopic("เบอร์โทร");
            viewgroup.setTextViewDetail(tel);



        }


    }
}
package com.fortunedental.fortunedental.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.fortunedental.fortunedental.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.Arrays;
import java.util.Calendar;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class AppointmentDateFragment extends Fragment {

    private MaterialCalendarView calendarSchedule;
    public static String[] splited_workdate;
    public static Integer[] workdate;
    MaterialCalendarView widget;

    public AppointmentDateFragment() {
        super();
    }

    public static AppointmentDateFragment newInstance() {
        AppointmentDateFragment fragment = new AppointmentDateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_appointment_date, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        calendarSchedule = (MaterialCalendarView) rootView.findViewById(R.id.calendarSchedule);



        String str = "2,3,7";
        splited_workdate = str.split(",");
        workdate = new Integer[splited_workdate.length];
        for(int i = 0;i < splited_workdate.length;i++)
        {
            // Note that this is assuming valid input
            // If you want to check then add a try/catch
            // and another index for the numbers if to continue adding the others
            workdate[i] = Integer.parseInt(splited_workdate[i]);
        }


        Calendar calendar = Calendar.getInstance();


        calendarSchedule.setSelectedDate(calendar.getTime());

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)-1, 20);
        calendarSchedule.setMinimumDate(calendar.getTime());

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)+3, 31);
        calendarSchedule.setMaximumDate(calendar.getTime());

        calendarSchedule.addDecorator(new EnableOneToTenDecorator());
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    private static class EnableOneToTenDecorator implements DayViewDecorator {

        private int dayofweek;
        private int first=0;
        private int dayDisable;
        private int dayToday;

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            this.dayToday=day.getDayToday();
            if(this.first==0) {
                this.dayofweek = day.getDayOfWeek();

                for(int i=this.dayToday;i>1;i--)
                {
                    if(this.dayofweek<=1)
                    {
                        this.dayofweek=7;
                        //Log.d("dayofweek-reset", String.valueOf(this.dayofweek));
                    }else
                    {
                        this.dayofweek--;
                        //Log.d("dayofweek-cal", String.valueOf(this.dayofweek));
                    }

                }
                //Log.d("Firstdayofweek", String.valueOf(this.dayofweek));
                this.first=1;
            }else
            {
                this.dayofweek++;
                if( this.dayofweek>7)
                {
                    this.dayofweek=1;
                }
            }

            if(Arrays.asList(workdate).contains(dayofweek))
            {
                this.dayDisable=day.getDay();

            }
            return day.getDay() !=this.dayDisable;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setDaysDisabled(true);
        }
    }

}

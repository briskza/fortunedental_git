package com.fortunedental.fortunedental.Customviewgroup;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.fortunedental.fortunedental.R;

/**
 * Created by acount on 26/4/2559.
 */
public class ListProfileCustomViewGroup extends FrameLayout {


    private TextView tvTopicCustomViewGroup;
    private TextView tvDetailCustomViewGroup;


    public ListProfileCustomViewGroup(Context context) {
        super(context);
        initInflate();
        initInsatances();
    }

    public ListProfileCustomViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInsatances();
    }

    public ListProfileCustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInsatances();
    }
    @TargetApi(21)
    public ListProfileCustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInsatances();
    }

    private void initInflate()
    {
        //Inflate layout here
        inflate(getContext(), R.layout.list_profile, this);
    }

    private void initInsatances()
    {
        // findViewByid here
        tvTopicCustomViewGroup = (TextView) findViewById(R.id.tvTopicCustomViewGroup);
        tvDetailCustomViewGroup = (TextView) findViewById(R.id.tvDetailCustomViewGroup);

    }
    public void setTextViewTopic(String text)
    {
        tvTopicCustomViewGroup.setText(text);
    }
    public void setTextViewDetail(String text)
    {
        tvDetailCustomViewGroup.setText(text);
    }
}

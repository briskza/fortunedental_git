package com.fortunedental.fortunedental.Customviewgroup;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fortunedental.fortunedental.R;

import org.w3c.dom.Text;

/**
 * Created by acount on 30/4/2559.
 */
public class ViewDescriptionScheduleCustomViewGroup extends FrameLayout {


    private TextView tvTopicDescriptionScheduleCustomViewGroup;
    private TextView tvDescriptionScheduleCustomViewGroup;
    private ImageView ivFirstaid;


    public ViewDescriptionScheduleCustomViewGroup(Context context) {
        super(context);
        initInflate();
        initInsatances();
    }

    public ViewDescriptionScheduleCustomViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInsatances();
    }

    public ViewDescriptionScheduleCustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInsatances();
    }
    @TargetApi(21)
    public ViewDescriptionScheduleCustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInsatances();
    }

    private void initInflate()
    {
        //Inflate layout here
        inflate(getContext(), R.layout.view_description_schedule, this);
    }

    private void initInsatances()
    {
        // findViewByid here
        tvTopicDescriptionScheduleCustomViewGroup= (TextView) findViewById(R.id.tvTopicDescriptionScheduleCustomViewGroup);
        tvDescriptionScheduleCustomViewGroup = (TextView) findViewById(R.id.tvDescriptionScheduleCustomViewGroup);
        ivFirstaid = (ImageView) findViewById(R.id.ivFirstaid);
    }

    public void setTextTopicCustomViewGroup(String text)
    {
        tvTopicDescriptionScheduleCustomViewGroup.setText(text);
    }

    public void setTexttvDescriptionCustomViewGroup(String text)
    {
        tvDescriptionScheduleCustomViewGroup.setText(text);
    }

    public void setHideivFirstaid()
    {
        ivFirstaid.setVisibility(INVISIBLE);
    }


}

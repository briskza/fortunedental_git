package com.fortunedental.fortunedental.Customviewgroup;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.fortunedental.fortunedental.R;

import org.w3c.dom.Text;

/**
 * Created by acount on 30/4/2559.
 */
public class ListDoctorScheduleCustomViewGroup extends FrameLayout {


    private TextView tvDateScheduleCustomViewGroup;
    private TextView tvTimeScheduleCustomViewGroup;
    private TextView tvNameScheduleCustomViewGroup;
    private TextView tvTypeScheduleCustomViewGroup;



    public ListDoctorScheduleCustomViewGroup(Context context) {
        super(context);
        initInflate();
        initInsatances();
    }

    public ListDoctorScheduleCustomViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInsatances();
    }

    public ListDoctorScheduleCustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInsatances();
    }
    @TargetApi(21)
    public ListDoctorScheduleCustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInsatances();
    }

    private void initInflate()
    {
        //Inflate layout here
        inflate(getContext(), R.layout.list_doctor_schedule, this);
    }

    private void initInsatances()
    {
        // findViewByid here
        tvDateScheduleCustomViewGroup = (TextView) findViewById(R.id.tvDateScheduleCustomViewGroup);
        tvTimeScheduleCustomViewGroup = (TextView) findViewById(R.id.tvTimeScheduleCustomViewGroup);
        tvNameScheduleCustomViewGroup = (TextView) findViewById(R.id.tvNameScheduleCustomViewGroup);
        tvTypeScheduleCustomViewGroup = (TextView) findViewById(R.id.tvTypeScheduleCustomViewGroup);

    }
    public void setTextViewDateSchedule(String text)
    {
        tvDateScheduleCustomViewGroup.setText(text);
    }
    public void setTextViewTimeSchedule(String text)
    {
        tvTimeScheduleCustomViewGroup.setText(text);
    }
    public void setTextViewNameSchedule(String text)
    {
        tvNameScheduleCustomViewGroup.setText(text);
    }
    public void setTextViewTypeSchedule(String text)
    {
        tvTypeScheduleCustomViewGroup.setText(text);
    }


}

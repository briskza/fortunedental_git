package com.fortunedental.fortunedental.util;

import android.app.Activity;

import com.fortunedental.fortunedental.DAO.DAO_DoctorSchedule;
import com.fortunedental.fortunedental.DAO.DAO_PatientSchedule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acount on 2/5/2559.
 */
public class JsonParser_PatientSchedule {
    public static List<DAO_PatientSchedule> parseSchedule(Activity a, JSONObject json) {
        List<DAO_PatientSchedule> list = new ArrayList<>();
        try {
            JSONArray jsonArray = json.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject j = jsonArray.getJSONObject(i);
                DAO_PatientSchedule model = new DAO_PatientSchedule();

                model.idPatient = j.getString("id_patient");
                model.date = j.getString("date");
                model.time = j.getString("time");
                model.name = j.getString("name");
                model.sname = j.getString("sname");
                model.type = j.getString("type");


                list.add(model);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
}

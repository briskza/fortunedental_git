package com.fortunedental.fortunedental.util;

import android.app.Activity;

import com.fortunedental.fortunedental.DAO.DAO_PatientProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acount on 25/4/2559.
 */
public class JsonParser_PatientProfile {

    public static List<DAO_PatientProfile> parseLogin(Activity a, JSONObject json) {
        List<DAO_PatientProfile> list = new ArrayList<>();
        try {
            JSONArray jsonArray = json.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject j = jsonArray.getJSONObject(i);
                DAO_PatientProfile model = new DAO_PatientProfile();
                model.idPatient = j.getString("id_patient");
                model.name = j.getString("name");
                model.sname = j.getString("sname");
                model.bday = j.getString("bday");
                model.age = j.getString("age");
                model.sex = j.getString("sex");
                model.address = j.getString("address");
                model.tel = j.getString("tel");
                model.idCard = j.getString("id_card");
                model.username = j.getString("username");
                model.password = j.getString("password");
                model.idDoctor = j.getString("id_doctor");

                list.add(model);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
}

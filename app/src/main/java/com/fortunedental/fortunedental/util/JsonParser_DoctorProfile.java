package com.fortunedental.fortunedental.util;

import android.app.Activity;

import com.fortunedental.fortunedental.DAO.DAO_DoctorProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acount on 1/5/2559.
 */
public class JsonParser_DoctorProfile {

    public static List<DAO_DoctorProfile> parseLogin(Activity a, JSONObject json) {
        List<DAO_DoctorProfile> list = new ArrayList<>();
        try {
            JSONArray jsonArray = json.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject j = jsonArray.getJSONObject(i);
                DAO_DoctorProfile model = new DAO_DoctorProfile();
                model.idDoctor = j.getString("id_doctor");
                model.name = j.getString("name");
                model.sname = j.getString("sname");
                model.bday = j.getString("bday");
                model.age = j.getString("age");
                model.sex = j.getString("sex");
                model.address = j.getString("address");
                model.tel = j.getString("tel");
                model.idCard = j.getString("id_card");
                model.username = j.getString("username");
                model.password = j.getString("password");


                list.add(model);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
}

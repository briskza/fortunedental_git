package com.fortunedental.fortunedental.util;

import android.app.Activity;

import com.fortunedental.fortunedental.DAO.DAO_DoctorSchedule;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acount on 2/5/2559.
 */
public class JsonParser_DoctorSchedule {
    public static List<DAO_DoctorSchedule> parseSchedule(Activity a, JSONObject json) {
        List<DAO_DoctorSchedule> list = new ArrayList<>();
        try {
            JSONArray jsonArray = json.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject j = jsonArray.getJSONObject(i);
                DAO_DoctorSchedule model = new DAO_DoctorSchedule();
                model.idPatient = j.getString("id_patient");
                model.idDoctor = j.getString("id_doctor");
                model.count = j.getString("count");
                model.date = j.getString("date");
                model.time = j.getString("time");
                model.type = j.getString("type");
                model.name = j.getString("name");
                model.sname = j.getString("sname");



                list.add(model);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
}

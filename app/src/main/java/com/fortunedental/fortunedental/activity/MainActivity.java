package com.fortunedental.fortunedental.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.fortunedental.fortunedental.DAO.DAO_DoctorSchedule;
import com.fortunedental.fortunedental.R;
import com.fortunedental.fortunedental.fragment.ContactFragment;
import com.fortunedental.fortunedental.fragment.ProfileFragment;
import com.fortunedental.fortunedental.fragment.ChatFragment;
import com.fortunedental.fortunedental.fragment.ScheduleFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity   {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private AlertDialog.Builder ad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ad = new AlertDialog.Builder(MainActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set hide action bar back
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                if(position==0)
//                {
//                    Toast.makeText(MainActivity.this,"Profile", Toast.LENGTH_SHORT).show();
//                }
//                else if(position==1)
//                {
//                    Toast.makeText(MainActivity.this,"Sehedule", Toast.LENGTH_SHORT).show();
//                }
//                else if(position==2)
//                {
//                    Toast.makeText(MainActivity.this,"Chat", Toast.LENGTH_SHORT).show();
//                }
//                else if(position==3)
//                {
//                    Toast.makeText(MainActivity.this,"Contact", Toast.LENGTH_SHORT).show();
//                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ProfileFragment(), "ประวัติส่วนตัว");
        adapter.addFragment(new ScheduleFragment(), "ตารางนัด");
        adapter.addFragment(new ChatFragment(), "แชท");
        adapter.addFragment(new ContactFragment(), "ข้อมูลติดต่อ");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_main_logout) {
            ad.setTitle("LOGOUT");
            ad.setMessage("คุณต้องการออกจากระบบใช่ไหม ?");
            ad.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            });
            ad.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                }
            });
            ad.show();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
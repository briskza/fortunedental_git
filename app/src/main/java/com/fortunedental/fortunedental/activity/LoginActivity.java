package com.fortunedental.fortunedental.activity;


import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.fortunedental.fortunedental.R;
import com.fortunedental.fortunedental.fragment.LoginFragment;


public class  LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (savedInstanceState == null) {
            //First Created
            //Place Fragment here
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainerLogin,
                            LoginFragment.newInstance(123),
                            "TagLoginFragment")
                    .commit();

        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        //activity communicate fragment tag
//        if (savedInstanceState == null) {
//            LoginFragment loginFragment = (LoginFragment)
//                    getSupportFragmentManager().findFragmentByTag("TagLoginFragment");
//            //loginFragment.setHelloText("eiei");
//
//        }
    }

}

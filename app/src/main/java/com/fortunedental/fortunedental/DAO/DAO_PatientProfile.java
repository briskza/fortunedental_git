package com.fortunedental.fortunedental.DAO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by acount on 25/4/2559.
 */
public class DAO_PatientProfile {

    @SerializedName("id_patient")
    @Expose
    public String idPatient;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("sname")
    @Expose
    public String sname;
    @SerializedName("bday")
    @Expose
    public String bday;
    @SerializedName("age")
    @Expose
    public String age;
    @SerializedName("sex")
    @Expose
    public String sex;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("tel")
    @Expose
    public String tel;
    @SerializedName("id_card")
    @Expose
    public String idCard;
    @SerializedName("username")
    @Expose
    public String username;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("id_doctor")
    @Expose
    public String idDoctor;
}

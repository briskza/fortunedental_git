package com.fortunedental.fortunedental.DAO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by acount on 2/5/2559.
 */
public class DAO_DoctorSchedule {

    @SerializedName("id_patient")   @Expose public String idPatient;
    @SerializedName("id_doctor")    @Expose public String idDoctor;
    @SerializedName("count")        @Expose public String count;
    @SerializedName("date")         @Expose public String date;
    @SerializedName("time")         @Expose public String time;
    @SerializedName("type")         @Expose public String type;
    @SerializedName("name")         @Expose public String name;
    @SerializedName("sname")        @Expose public String sname;

}

package com.fortunedental.fortunedental.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fortunedental.fortunedental.Customviewgroup.ListDoctorScheduleCustomViewGroup;
import com.fortunedental.fortunedental.DAO.DAO_DoctorSchedule;

import java.util.List;

/**
 * Created by acount on 30/4/2559.
 */
public class ScheduleDoctorListAdapter extends BaseAdapter {
    List<DAO_DoctorSchedule> data;

    public ScheduleDoctorListAdapter(List data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        if (data == null) {
            return 0;
        } else {
            return this.data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListDoctorScheduleCustomViewGroup item;

        //เอาของที่ออกจากรายการนำกลับมาใช้ใหม่
        if (convertView != null) {
            item = (ListDoctorScheduleCustomViewGroup) convertView;
        } else {
            //ถ้ายังไม่มีรายการ สร้างใหม่
            item = new ListDoctorScheduleCustomViewGroup(parent.getContext());
        }

        item.setTextViewDateSchedule(data.get(position).date);
        item.setTextViewTimeSchedule("เวลานัด " + data.get(position).time);
        item.setTextViewNameSchedule("คุณ " + data.get(position).name + " " + data.get(position).sname);
        item.setTextViewTypeSchedule("รายการตรวจ :"+data.get(position).type);
        return item;
    }
}

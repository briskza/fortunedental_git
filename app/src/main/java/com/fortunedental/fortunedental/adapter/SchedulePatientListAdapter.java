package com.fortunedental.fortunedental.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fortunedental.fortunedental.Customviewgroup.ListDoctorScheduleCustomViewGroup;
import com.fortunedental.fortunedental.Customviewgroup.ListPatientScheduleCustomViewGroup;
import com.fortunedental.fortunedental.DAO.DAO_DoctorSchedule;
import com.fortunedental.fortunedental.DAO.DAO_PatientSchedule;

import java.util.List;

/**
 * Created by acount on 30/4/2559.
 */
public class SchedulePatientListAdapter extends BaseAdapter {
    List<DAO_PatientSchedule> data;

    public SchedulePatientListAdapter(List data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        if (data == null) {
            return 0;
        } else {
            return this.data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListPatientScheduleCustomViewGroup item;

        //เอาของที่ออกจากรายการนำกลับมาใช้ใหม่
        if (convertView != null) {
            item = (ListPatientScheduleCustomViewGroup) convertView;
        } else {
            //ถ้ายังไม่มีรายการ สร้างใหม่
            item = new ListPatientScheduleCustomViewGroup(parent.getContext());
        }

        item.setTextViewDateSchedule(data.get(position).date);
        item.setTextViewTimeSchedule("เวลานัด " + data.get(position).time);
        item.setTextViewTypeSchedule("ตรวจ :" + data.get(position).type);
        item.setTextViewNameSchedule("แพทย์ผู้ดูแล" + data.get(position).name + " " + data.get(position).sname);
        return item;
    }
}
